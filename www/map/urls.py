"""URLs for the map project."""

from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import TemplateView

from main import views as main_views
from main import api_urls as main_api_urls


urlpatterns = [
    path('', main_views.welcome, name='welcome'),
    path('map/', main_views.map, name='map'),
    path('map/<share_code>/', main_views.map, name='map'),
    path('follow/<share_code>/', main_views.follow, name='follow'),
    path('register/', main_views.register, name='register'),
    path('track/', main_views.track, name='tracking'),
    path('friends/', main_views.friends, name='friends'),
    path('settings/', main_views.settings, name='settings'),
    path('goodbye/', main_views.goodbye, name='goodbye'),
    path('admin/', admin.site.urls),
    path('api/', include((main_api_urls, 'main'), namespace='api')),
]
