"""Management command to delete users that have been inactive for a period."""

from datetime import timedelta

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from main import models


class Command(BaseCommand):
    """Management command to delete old, inactive users."""

    help = 'Delete old, inactive users.'

    def handle(self, *args, **kwargs):
        """Do the dirty job."""
        users = models.MapUser.objects.filter(
            last_activity__lt=timezone.now() - timedelta(days=30)
        )
        user_count = users.count()
        users.delete()
        self.stdout.write('{} users were deleted.'.format(user_count))
