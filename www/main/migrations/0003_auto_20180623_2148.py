# Generated by Django 2.0.6 on 2018-06-23 21:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_mapuser_colour'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mapuser',
            old_name='colour',
            new_name='color',
        ),
    ]
