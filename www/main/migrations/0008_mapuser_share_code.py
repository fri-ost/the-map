# Generated by Django 2.0.6 on 2018-06-24 22:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20180624_2046'),
    ]

    operations = [
        migrations.AddField(
            model_name='mapuser',
            name='share_code',
            field=models.CharField(db_index=True, max_length=6, null=True, unique=True),
        ),
    ]
