"""Models for the main app."""

from datetime import datetime, timedelta
import random
import string

from django.db import models
from django.utils import timezone


class MapUser(models.Model):
    """
    A user in the map project.

    We're deliberately not using the regular Django user model, as users in
    this context are usually ephemeral, and only exists during a shorter period
    of time.
    """

    initials = models.CharField(max_length=3)
    nickname = models.CharField(max_length=100)
    color = models.CharField(max_length=11, default='127,127,127')
    share_code = models.CharField(
        max_length=6,
        unique=True,
        db_index=True,
        null=True
    )
    last_activity = models.DateTimeField(auto_now=True)

    def generate_share_code(self):
        """Generates a share code."""
        return ''.join(random.choices(
            string.ascii_lowercase + string.digits, k=6
        ))

    def save(self, *args, **kwargs):
        """Override save to set a unique share code."""
        if self.share_code is None:
            code = self.generate_share_code()
            while MapUser.objects.filter(share_code=code).exists():
                code = self.generate_share_code()
            self.share_code = code
        super().save(*args, **kwargs)

    @property
    def current_track(self):
        """Get the active track, if any."""
        return self.track_set.filter(
            end__gt=timezone.now()
        ).first()

    def start_tracking(self, duration):
        """Initialize the location tracking of the user."""
        track = Track.objects.create(
            user=self,
            end=timezone.now() + timedelta(seconds=duration)
        )

    def stop_tracking(self):
        """Stop the location tracking of the user."""
        self.current_track.delete()


class Track(models.Model):
    """
    A track is a single tracking session.

    For now, it will just know about the most recent reported location during
    the tracking session. But later on, we might keep track of all reported
    location, to be able to present an actual track on the map.
    """

    user = models.ForeignKey(MapUser, on_delete=models.CASCADE)
    start = models.DateTimeField(auto_now_add=True)
    end = models.DateTimeField()
    last_updated = models.DateTimeField(default=None, null=True)

    @property
    def location(self):
        """Get the most recent reported location."""
        return self.location_set.order_by('-time').first()


class Location(models.Model):
    """A location is a place in time and space, related to a track."""

    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now=True)
    longitude = models.FloatField()
    latitude = models.FloatField()
