from django import forms

from . import models


class MapUserForm(forms.ModelForm):
    """A form for map users."""

    class Meta:
        """Meta class for the model form."""
        model = models.MapUser
        fields = [
            'nickname',
            'initials',
            'color',
        ]
        widgets = {
            'color': forms.HiddenInput,
        }
