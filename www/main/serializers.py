"""Serializers for the main app."""

from django.utils import timezone
from rest_framework import serializers

from . import models


class MapUserSerializer(serializers.ModelSerializer):
    """A serializer for map users."""

    class Meta:
        model = models.MapUser
        fields = [
            'nickname',
            'initials',
            'color',
        ]


class LocationSerializer(serializers.ModelSerializer):
    """A serializer for location tracking."""

    class Meta:
        model = models.Location
        fields = [
            'track_id',
            'time',
            'latitude',
            'longitude',
        ]
        read_only_fields = [
            'track_id',
            'time',
        ]
