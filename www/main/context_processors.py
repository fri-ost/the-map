"""Context processors for the main app."""

from . import models


def default_map_context(request):
    """Add some context variables that we want to have everywhere."""
    # Get the user, if any.
    user = models.MapUser.objects.filter(
        pk=request.session.get('user_id')
    ).first()
    # Save the user to update last activity time stamp.
    if user:
        user.save()

    # Get the followers, if any.
    following = models.MapUser.objects.filter(
        share_code__in=request.session.get('following')
    ) if request.session.get('following') else None
    return {
        'user': user,
        'following': following,
    }
