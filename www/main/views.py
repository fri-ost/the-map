"""Views for the main app."""

from django.contrib import messages
from django.shortcuts import (
    get_object_or_404,
    redirect,
    render,
)
from django.urls import reverse

from . import (
    forms,
    models,
)


def welcome(request):
    """The front page."""
    return render(request, 'index.html', {
    })

def map(request, share_code=None):
    """The actual map."""
    to_follow = None
    if share_code is not None:
        to_follow = get_object_or_404(models.MapUser, share_code=share_code)
        if request.session.get('following'):
            if share_code not in request.session.get('following'):
                request.session.get('following').append(share_code)
        else:
            request.session['following'] = [share_code,]

    return render(request, 'map.html', {
        'to_follow': to_follow,
    })

def follow(request, share_code):
    """Follow a friend on the map."""
    to_follow = get_object_or_404(models.MapUser, share_code=share_code)
    if request.session.get('following'):
        if share_code not in request.session.get('following'):
            request.session.get('following').append(share_code)
    else:
        request.session['following'] = [share_code,]

    return render(request, 'follow.html', {
        'to_follow': to_follow,
    })

def friends(request):
    """A friend in need's a friend indeed."""
    return render(request, 'friends.html', {
    })

def register(request):
    """Register as a map user."""
    if request.method == 'POST':
        form = forms.MapUserForm(request.POST)
        if form.is_valid():
            # Delete existing user, if any.
            user = models.MapUser.objects.filter(
                pk=request.session.get('user_id')
            ).first()
            if user is not None:
                user.delete()

            # Create new user.
            user = form.save()
            request.session['user_id'] = user.id
            return redirect(reverse('welcome'))
    else:
        form = forms.MapUserForm()
    return render(request, 'register.html', {
        'form': form,
    })

def settings(request):
    """Change user settings."""
    user = models.MapUser.objects.filter(
        pk=request.session.get('user_id')
    ).first()

    if user is None:
        return redirect(reverse('register'))

    if request.method == 'POST':
        form = forms.MapUserForm(request.POST, instance=user)
        if form.is_valid():
            user = form.save()
            request.session['user_id'] = user.id
            return redirect(reverse('welcome'))
    else:
        form = forms.MapUserForm(instance=user)
    return render(request, 'settings.html', {
        'form': form,
    })

def track(request):
    """Change the tracking settings."""
    user = models.MapUser.objects.filter(
        pk=request.session.get('user_id')
    ).first()

    if user is None:
        return redirect(reverse('register'))

    if request.method == 'POST':
        action = request.POST.get('action')
        if action:
            action = action.upper()

        if action == 'START TRACKING':
            if user.current_track:
                messages.add_message(
                    request,
                    messages.INFO,
                    'Your location is already being tracked.'
                )
            else:
                duration = request.POST.get('duration')
                try:
                    duration = int(duration)
                    user.start_tracking(duration)
                    messages.add_message(
                        request,
                        messages.SUCCESS,
                        'The tracking of your location has started.'
                    )
                except ValueError:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        (
                            'The selected duration "{}" is not valid. Try '
                            'something else.'
                        ).format(duration)
                    )

        elif action == 'STOP TRACKING':
            if user.current_track is None:
                messages.add_message(
                    request,
                    messages.INFO,
                    'We can\'t stop tracking, as your location is not being '
                    'tracked right now.'
                )
            else:
                user.stop_tracking()
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    'The tracking of your location has been stopped. '
                    'All location data has been deleted.'
                )
        else:
            messages.add_message(
                request,
                messages.ERROR,
                'We don\'t really know what you mean by '
                'the action "{}".'.format(action)
            )

        return redirect(reverse('tracking'))

    return render(request, 'tracking.html', {
        'durations': [
            (60 * 5, '5 minutes'),
            (60 * 15, '15 minutes'),
            (60 * 30, '30 minutes'),
            (60 * 60, 'An hour'),
        ],
    })

def goodbye(request):
    """Delete the active user."""
    user = models.MapUser.objects.filter(
        pk=request.session.get('user_id')
    ).first()

    if user is None:
        return redirect(reverse('welcome'))

    if request.method == 'POST':
            user.delete()
            request.session.pop('user_id')
            return redirect(reverse('welcome'))

    return render(request, 'goodbye.html', {
    })
