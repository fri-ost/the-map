"""API endpoints for the main app."""

from django.utils import timezone
from rest_framework.decorators import (
    api_view,
    permission_classes,
)
from rest_framework import exceptions
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import BasePermission
from rest_framework.response import Response
from rest_framework import status

from . import (
    models,
    serializers,
)


class MapAuthenticatedPermission(BasePermission):
    """A custom permission class for The Map."""

    def has_permission(self, request, view):
        """
        Determine general access permission.

        It's a hack, but if permission is granted, the `map_user` is also
        assigned to the request here.
        """
        user = models.MapUser.objects.filter(
            pk=request.session.get('user_id')
        ).first()

        if user is None:
            return False
        else:
            request.map_user = user
            return True

    def has_object_permission(self, request, view, obj):
        """Determine object permission."""
        return False


@api_view(['GET',])
def user_data(request, share_code):
    """Get info about a user."""
    user = get_object_or_404(models.MapUser, share_code=share_code)
    serializer = serializers.MapUserSerializer(user)
    return Response(serializer.data)

@api_view(['POST',])
@permission_classes((MapAuthenticatedPermission,))
def track(request):
    """Register the location of the user."""
    track = request.map_user.current_track
    if track is None:
        raise exceptions.ValidationError(
            'You are not allowed to report your location, as you are not '
            'currently tracking your location.'
        )
    if track.location is None:
        models.Location.objects.create(track=track, latitude=0, longitude=0)
    serializer = serializers.LocationSerializer(
        track.location,
        data=request.data
    )
    if serializer.is_valid(raise_exception=True):
        serializer.save()
        track.last_updated = timezone.now()
        track.save()
    return Response(serializer.data)


@api_view(['GET',])
def track_code(request, share_code):
    """Get the location of another user by code."""
    to_follow = get_object_or_404(models.MapUser, share_code=share_code)
    if to_follow.current_track and to_follow.current_track.location:
        location = to_follow.current_track.location
    else:
        return Response(status=status.HTTP_204_NO_CONTENT)
    serializer = serializers.LocationSerializer(
        to_follow.current_track.location
    )
    return Response(serializer.data)
