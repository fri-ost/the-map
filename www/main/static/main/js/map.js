var color_channel_dec_to_hex = function(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

var color_dec_to_hex = function(c) {
    var channels = c.split(',');
    var output = '#'
    for (ch of channels) {
        output += color_channel_dec_to_hex(parseInt(ch));
    }
    return output;
}

var color_shuffle = function() {
    var rgb = [0,0,0];
    var channel = Math.floor(Math.random() * 3)
    for (var i = 0; i < 3; i++) {
        if (i != channel) {
            rgb[i] = 127 + Math.floor(Math.random() * 127)
        } else {
            rgb[i] = 63 + Math.floor(Math.random() * 63)
        }
    }
    return {
        rgb: rgb,
        value: rgb.join(','),
        style: 'rgb(' + rgb.join(',') + ')',
    }
}

var init_messages = function() {
    var messages = document.querySelector('#messages');
    if (messages != null) {
        messages.addEventListener('click', function(e) {
            messages.remove();
        });
    }
}

var init_user_form = function(do_color_shuffle) {
    if (do_color_shuffle == true) {
        var rgb = color_shuffle();
        document.querySelector('#id_color').value = rgb.value;
    }
    var color_picker = document.createElement('li');
    var current_color = document.querySelector('#id_color').value;
    var color_name = ntc.name(color_dec_to_hex(current_color));
    color_picker.classList.add('color-picker');
    color_picker.innerHTML = '<label>Your colour:</label><span>' + color_name[1] + '</span> '
    color_picker.getElementsByTagName('span')[0].style.background = 'rgb(' + current_color + ')';
    var shuffle_trigger = document.createElement('a');
    shuffle_trigger.href = '.';
    shuffle_trigger.innerText = 'change';
    shuffle_trigger.addEventListener('click', function(e) {
        e.preventDefault();
        shuffle_trigger.blur()
        var rgb = color_shuffle();
        color_name = ntc.name(color_dec_to_hex(rgb.value));
        color_picker.querySelector('span').style.background = rgb.style;
        color_picker.querySelector('span').innerText = color_name[1];
        document.querySelector('#id_color').value = rgb.value;
    });
    color_picker.appendChild(shuffle_trigger);
    document.querySelector('form ul').insertBefore(
        color_picker,
        document.querySelector('form ul li:last-child')
    )
}

var create_dot = function(label, color, title) {
    var dot = document.createElement('div');
    dot.classList.add('dot');
    dot.title = title || label
    dot.style.background = 'rgba(' + color + ', 80%)';
    dot.innerText = label;
    return dot;
}

var create_control = function(classes, label, title, color, click) {
    var wrapper = document.createElement('div');
    wrapper.className = 'ol-control ol-unselectable ' + classes;
    wrapper.innerHTML = '<button title="' + title + '">' + label + '</button>';
    var button = wrapper.querySelector('button');
    button.addEventListener('click', click);
    if (color) {
        button.style.background = 'rgba(' + color + ', 80%)';
    }
    var control = new ol.control.Control({
        element: wrapper,
    });
    return control;
}

var center_map = function(map, coords) {
    if (coords) {
        var pos = ol.proj.fromLonLat([
            coords.longitude,
            coords.latitude
        ]);
        map.getView().setCenter(pos);
    }
}

var show_banner = function(message) {
    var banner = document.getElementById('map-banner');
    banner.innerHTML = message;
    banner.style.display = 'block';
}

var hide_banner = function() {
    var banner = document.getElementById('map-banner');
    banner.style.display = 'none';
}

var colored_user = function(user, as_tag) {
    if (as_tag) {
        var tag = document.createElement('span');
        tag.style.color = 'rgb(' + user.color + ');';
        tag.innerText = user.nickname;
        return tag;
    } else {
        return '<span style="color: rgb(' + user.color + ');">' + user.nickname + '</span>';
    }
}

var init_map = function() {
    var path = window.location.pathname;
    var re = /\/map\/(\w{6})\/?/;
    var follow_match = path.match(re);
    if (follow_match) {
        follow_code = follow_match[1];
        var req = new XMLHttpRequest();
        req.addEventListener('load', function() {
            if (this.status == 404) {
                show_banner('You\'re trying to follow a user that does not exist.');
            } else {
                to_follow = this.response;
                to_follow.position = null;
                show_banner('You\'re following ' + colored_user(to_follow) + '. Locking on target...');

                var dot = new ol.Overlay({
                    id: 'dot-follow',
                    element: create_dot(to_follow.initials, to_follow.color, 'This is the location of ' + to_follow.nickname),
                    positioning: 'center-center',
                });
                var center_control = create_control('control-center-map control-center-follow', '⌖', 'Center map on ' + to_follow.nickname, to_follow.color, function(e) {
                    center_map(map, to_follow.position);
                });
                var visible = false;

                var get_position = function(is_first_fix) {
                    var req = new XMLHttpRequest();
                    req.addEventListener('load', function() {
                        if (this.status == 204) {
                            show_banner('The location of ' + colored_user(to_follow) + ' is currently not being shared.');
                            is_first_fix = false;
                            if (visible == true) {
                                map.getOverlays().remove(dot);
                                map.getControls().remove(center_control);
                                visible = false;
                            }
                        }
                        else if (this.status == 200) {
                            show_banner('Following the location of ' + colored_user(to_follow) + '.');
                            to_follow.position = this.response;

                            if (is_first_fix) {
                                center_map(map, to_follow.position);
                                is_first_fix = false;
                            }
                            if (visible == false) {
                                map.addControl(center_control);
                                var me = null;
                                if (map.getOverlays().getLength()) {
                                    me = map.getOverlays().removeAt(0);
                                }
                                map.getOverlays().insertAt(0, dot);
                                if (me) {
                                    map.getOverlays().insertAt(1, me);
                                }
                                visible = true;
                            }
                            dot.setPosition(ol.proj.fromLonLat([
                                to_follow.position.longitude,
                                to_follow.position.latitude
                            ]));
                        } else {
                            show_banner('Something is wrong. Hang on for a while or try refreshing the page.');
                        }
                        window.setTimeout(function() {
                            get_position(is_first_fix);
                        }, 5000);
                    })
                    req.open('GET', '/api/track/' + follow_code + '/');
                    req.setRequestHeader('Content-Type', 'application/json');
                    req.responseType = 'json';
                    req.send();
                }
                get_position(true);
        if (track_position) {
            report_position();
        }
            }
        })
        req.open('GET', '/api/user/' + follow_code + '/');
        req.setRequestHeader('Content-Type', 'application/json');
        req.responseType = 'json';
        req.send();
    }

    var me = new ol.Overlay({
        id: 'dot-me',
        element: create_dot(user.initials || 'YOU', user.color, 'This is your own location'),
        positioning: 'center-center',
    });

    var map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([
                12.07720,
                55.62171
            ]),
            zoom: 18
        }),
        controls: ol.control.defaults({
            attribution: false,
        }),
    });
    map.addControl(create_control('control-center-map', '⌖', 'Center map on your own location', null, function(e) {
        center_map(map, user.position);
    }));
    map.getOverlays().insertAt(1, me);

    if (!('geolocation' in navigator)) {
        alert('Your browser does not support geo location. You are only able to view the location of others, not share your own location.');
    } else {
        var map_watch_id = navigator.geolocation.watchPosition(function(position) {
            // Center map on first GPS fix if we're not following anyone.
            if (!user.position && to_follow == null) {
                center_map(map, position.coords);
            }
            user.position = position.coords;
            var pos = ol.proj.fromLonLat([
                position.coords.longitude,
                position.coords.latitude
            ]);
            me.setPosition(pos);
        }, null, {
            enableHighAccuracy: true,
            maximumAge: 10000,
        });
    }
}

var stop_tracking = function() {
    track_position = false;
    document.getElementById('tracking-indicator').classList.remove('active');
    document.getElementById('tracking-indicator').title = 'Tracking (not enabled)';
    if (window.location.pathname == '/track/') {
        window.location.reload();
    }
}

var init_tracking = function() {
    if (track_position) {
        document.getElementById('tracking-indicator').classList.add('active');
    }
        var report_position = function() {
            navigator.geolocation.getCurrentPosition(function(position) {
                var req = new XMLHttpRequest();
                req.addEventListener('load', function() {
                    if (this.status == 400 || this.status == 403) {
                        stop_tracking();
                    }
                    if (track_position) {
                        window.setTimeout(report_position, 5000);
                    }
                })
                req.open('POST', '/api/track/');
                req.setRequestHeader('Content-Type', 'application/json');
                req.responseType = 'json';
                req.send(JSON.stringify({
                    longitude: position.coords.longitude.toFixed(6),
                    latitude: position.coords.latitude.toFixed(6),
                }));
            }, null, {
                enableHighAccuracy: true,
                maximumAge: 10000,
            });
        }
        if (track_position) {
            report_position();
        }
}

var init_share_urls = function() {
    var urls = document.querySelectorAll('.share-url');
    if (urls.length) {
        for (var u in urls) {
            if (parseInt(u) > -1) {
                urls[u].addEventListener('focus', function(e) {
                    e.target.select();
                });
            }
        }
    }
}

window.addEventListener('load', function() {
    init_messages();
    init_tracking();
    init_share_urls();
    if (document.body.classList.contains('map')) {
        init_map();
    }
    if (document.body.classList.contains('user-form')) {
        init_user_form(
            document.body.classList.contains('register')
        );
    }
});
