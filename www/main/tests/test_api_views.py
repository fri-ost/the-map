"""Tests for the API views."""

from datetime import timedelta
import pytz

from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.test import (
    APIClient,
    APITestCase,
)

from .. import models


class BaseTestCase(APITestCase):
    """A base class for test classes."""

    def setUp(self):
        """Set up test data."""
        self.client = APIClient()

    def register(self):
        """A shortcut to get the client registered."""
        response = self.client.post(reverse('register'), {
            'nickname': 'test',
            'initials': 'tst',
            'color': '1,2,3',
        })
        self.assertEqual(
            response.status_code,
            status.HTTP_302_FOUND
        )
        self.assertIn(
            'user_id',
            self.client.session.keys()
        )
        return models.MapUser.objects.get(
            pk=self.client.session.get('user_id')
        )


class UserDataTestCase(BaseTestCase):
    """Tests for the user data endpoint."""

    def test_404(self):
        """Test that non-existent users return 404."""
        response = self.client.get(reverse('api:user_data', kwargs={
            'share_code': '123456',
        }))
        self.assertEqual(
            response.status_code,
            status.HTTP_404_NOT_FOUND
        )

    def test_user_data(self):
        """Test that the returned user data looks right."""
        # Create a user, but don't be that user.
        user = self.register()
        self.client.session['user_id'] = None

        # Get the user data
        response = self.client.get(reverse('api:user_data', kwargs={
            'share_code': user.share_code,
        }))
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )

        # Check that the user data looks right.
        self.assertEqual(
            response.data.get('nickname'),
            user.nickname
        )
        self.assertEqual(
            response.data.get('initials'),
            user.initials
        )
        self.assertEqual(
            response.data.get('color'),
            user.color
        )


class TrackTestCase(BaseTestCase):
    """Tests for the tracking endpoint."""

    def test_not_registered(self):
        """Check that unregistered visitors get an error."""
        response = self.client.get(reverse('api:track'))
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN
        )

    def test_not_tracking(self):
        """Check that tracking while user is not tracking gives an error."""
        user = self.register()
        data = {
            'latitude': 22.22,
            'longitude': 22.22,
        }
        response = self.client.post(reverse('api:track'), data, format='json')
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST
        )

    def test_tracking(self):
        """Check that tracking works as expected."""
        user = self.register()
        user.start_tracking(3600)

        self.assertIsNone(user.current_track.location)

        data = {
            'latitude': 22.22,
            'longitude': 22.22,
        }
        response = self.client.post(reverse('api:track'), data, format='json')
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )


class CodeTrackTestCase(BaseTestCase):
    """Tests for tracking a share code."""

    def test_404(self):
        """Test that an invalid share code returns 404."""
        response = self.client.get(reverse('api:track_code', kwargs={
            'share_code': '123456'
        }))
        self.assertEqual(
            response.status_code,
            status.HTTP_404_NOT_FOUND
        )

    def test_location(self):
        """Test that it's possible to get the location of others by code."""
        # Create a user to get location for, but make sure we're not logged
        # in as this user.
        user = self.register()
        self.client.session['user_id'] = None
        # Make sure that a user without a track returns nothing.
        response = self.client.get(
            reverse('api:track_code', kwargs={
                'share_code': user.share_code,
            }),
            format='json'
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_204_NO_CONTENT
        )
        track = models.Track.objects.create(
            user=user,
            start=timezone.now(),
            end=timezone.now() + timedelta(minutes=10)
        )
        # Make sure that a user with a track without a location
        # returns nothing.
        self.assertEqual(
            response.status_code,
            status.HTTP_204_NO_CONTENT
        )

        # Finally, create a location and check that it's returned properly.
        latitude = 22.22
        longitude = 33.33
        location = models.Location.objects.create(
            track=track,
            latitude=latitude,
            longitude=longitude
        )
        response = self.client.get(
            reverse('api:track_code', kwargs={
                'share_code': user.share_code,
            }),
            format='json'
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )
        self.assertEqual(
            response.data.get('latitude'),
            latitude
        )
        self.assertEqual(
            response.data.get('longitude'),
            longitude
        )
        local_timezone = pytz.timezone(settings.TIME_ZONE)
        self.assertEqual(
            response.data.get('time'),
            location.time.astimezone(local_timezone).isoformat()
        )
