"""API URLs for the main app."""

from django.urls import path

from . import api_views


urlpatterns = [
    path('user/<share_code>/', api_views.user_data, name='user_data'),
    path('track/', api_views.track, name='track'),
    path('track/<share_code>/', api_views.track_code, name='track_code'),
]
